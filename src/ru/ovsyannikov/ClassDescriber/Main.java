package ru.ovsyannikov.ClassDescriber;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        ClassDescriber listMaker = new ClassDescriber();
        List<Object> list = new ArrayList<>();

        list.add(new Person("Tom", "Hawk", LocalDate.of(2018, 3, 9), "hawk@gmail.com"));
        list.add(new Person("Ben", "Stiller", LocalDate.of(2018, 3, 9)));
        list.add(new Person("Carl", "Jonson"));

        for (List<Description> lizt : listMaker.makeListDescriptionClass(list)) {
            System.out.println(lizt.toString());
        }
    }



}
