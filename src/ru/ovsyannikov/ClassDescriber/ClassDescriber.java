package ru.ovsyannikov.ClassDescriber;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClassDescriber {
    public static final Logger logger = Logger.getLogger(ClassDescriber.class.getName());
    public List<List<Description>> makeListDescriptionClass(List<Object> objects) {
        List<List<Description>> result = new ArrayList<>();
        Class<?> clazz1 = objects.get(0).getClass();
        for (Object object : objects) {
            if (clazz1 != object.getClass()){
                throw new IllegalArgumentException();
            }
            Class<?> clazz = object.getClass();
            do {
                List<Description> childClass = new ArrayList<>();
                describeClass(clazz.getDeclaredFields(), childClass, object);
                if (childClass.size() > 0) result.add(childClass);
                clazz = clazz.getSuperclass();
            } while (clazz.getSuperclass() != null);
        }
        return result;

    }

    private void describeClass(Field[] fields, List<Description> childClass, Object object) {
        for (Field field : fields) {
            boolean hasValue = false;
            try {
                field.setAccessible(true);
                if (field.get(object) != null) {
                    hasValue = true;
                }
                childClass.add(new Description(field.getName(), field.getType().toString(), hasValue));
            } catch (IllegalAccessException e) {
                logger.log(Level.ALL,e.getMessage());
            }
        }
    }

}
