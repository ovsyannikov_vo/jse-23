package ru.ovsyannikov.ClassDescriber;

public class Description {
    public String parameterName;
    public String parameterTypeName;
    public Boolean hasValue;

    public Description(String parameterName, String parameterTypeName, Boolean hasValue) {
        this.parameterName = parameterName;
        this.parameterTypeName = parameterTypeName;
        this.hasValue = hasValue;
    }

    @Override
    public String toString() {
        return "Description{" +
                "parameterName='" + parameterName + '\'' +
                ", parameterTypeName='" + parameterTypeName + '\'' +
                ", hasValue=" + hasValue +
                '}';
    }

}
